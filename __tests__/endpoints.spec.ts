
import * as request from 'supertest';
import book from '../src/routes/book';
import { createConnection, getConnection } from 'typeorm';

beforeEach(() => {
    return createConnection({
        "type": "mysql",
        "host": "localhost",
        "port": 3306,
        "username": "test",
        "password": "test",
        "database": "books",
        "entities": ["dist/entity/*.js"],
        "logging": true,
        "synchronize": true
    });
});

afterEach(() => {
    const conn = getConnection();
    return conn.close();
});

describe('test the test', () => {
    test('check this works', () => {
        const ok = 'ok';
        expect(ok).toEqual('ok');
    })
});

describe('test api', () => {
    test('test connection to api', async () => {
        const response = await request(book).get('/book');
        expect(response.statusCode).toEqual(200);
    })
})

describe('test api', () => {
    test('test book isbn', async () => {
        const response = await request(book).get('/book/123468125102');
        expect(response.statusCode).toEqual(200);
    })
})

describe('test api', () => {
    test('test book fail endpoint isbn', async () => {
        const response = await request(book).get('/book/1234681251025161561');
        expect(response.statusCode).toEqual(400);
    })
})
