import { palindromo } from '../src/BonusCuestionario/palindromo';
import { multiplos } from '../src/BonusCuestionario/multiplos';
import { sumaEnteros } from '../src/BonusCuestionario/sumaEnteros';
import { Persona } from '../src/BonusCuestionario/persona';

describe('test palindromo', () => {
    test('test palindromo', () => {
        const text = 'Ali tomo tila';
        const res = palindromo(text);
        expect(res).toBeTruthy();
    })
})

describe('test fail palindromo', () => {
    test('test fail palindromo', () => {
        const text = 'putilla';
        const res = palindromo(text);
        expect(res).toBe(false);
    })
})

describe('test multiplos', () => {
    test('test multiplos', () => {
        const multiplosList = [7,14,28,50,23];
        const res = multiplos(multiplosList);
        const ok = [7,14,28];
        expect(res).toEqual(ok);
    })
})


describe('test suma enteros', () => {
    test('test suma enteros', () => {
        const enteros = [7,14,28,50,23];
        const res = sumaEnteros(enteros);
        const ok = 122;
        expect(res).toEqual(ok);
    })
})

describe('test suma enteros fail', () => {
    test('test suma enteros fail', () => {
        const enteros = [7, 152.52,28,50,23];
        const res = sumaEnteros(enteros);
        
        expect(res).toBeNull();
    })
})

describe('test persona return name age', () => {
    test('test persona return name age', () => {
        const persona = Object.assign(new Persona(), {
            "name": "John",
            "surname": "Doe",
            "age": "30",
            "country": "Spain",
            "city": "Madrid",
            "employed": "true",
            "dni": "00000000A",
            });
        
        const res = persona.returnNameNAge(persona);
        console.log(res);
        expect(res).toEqual({ "name": "John", "age": "30" });
    })
})

describe('test persona remove employed and dni', () => {
    test('test persona remove employed and dni', () => {
        const persona = Object.assign(new Persona(), {
            "name": "John",
            "surname": "Doe",
            "age": "30",
            "country": "Spain",
            "city": "Madrid",
            "employed": "true",
            "dni": "00000000A",
            });
        
        const res = persona.returnPersonaWithOutEmployedNDni(persona);
        expect(res).toEqual({
            "name": "John",
            "surname": "Doe",
            "age": "30",
            "country": "Spain",
            "city": "Madrid",
            });
    })
})
