import { Checks } from '../src/utils/checks';
import { BookPost } from '../src/entryDto/BookPost.dto';


describe('test the test', () => {
    test('check this works', () => {
        const ok = 'ok';
        expect(ok).toEqual('ok');
    })
});

describe('test utils', () => {
    test('utils', () => {
        
    });
})

describe('check object BookPost', () => {
    test('check object BookPost', () => {
        const mock = { 
            isbn: 'abcded',
            title: 'some title',
            author: 'some author',
            subject: 'some subject',
            birthDate: '12-03-1979'
        }

        const post = Object.assign(new BookPost(), mock);
        const result = Checks.checkBookPost(post);
        expect(result).toBeTruthy();
    });
})

describe('check object isnt a BookPost', () => {
    test('check object isnt a BookPost', () => {
        const mock = { 
            isbn: 'abcded',
            title: 'some title',
            author: 'some author',
            subject: 'some subject',
        }

        const post = Object.assign(new BookPost(), mock);
        const result = Checks.checkBookPost(post);
        expect(!result).toBeTruthy();
    });
})

describe('valid isbn string', () => {
    test('valid isbn string', () => {
        const isbnTest = '12345678910';
        const result = Checks.validIsbn(isbnTest);
        expect(result).toBeTruthy();
    });
})

describe('invalid isbn string', () => {
    test('invalid isbn string', () => {
        const isbnTest = '1234';
        const result = Checks.validIsbn(isbnTest);
        expect(!result).toBeTruthy();
    });
})



