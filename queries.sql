INSERT INTO books.book
(isbn, title, authorId)
VALUES(9788445007709, 'el señor de los porrillos', NULL);


SELECT *
FROM books.book as b
LEFT JOIN books.book_book_subject_book_subject as bsj ON bsj.bookIsbn = b.isbn
LEFT JOIN books.author as a ON a.id = b.authorId 
LEFT JOIN books.book_subject as bs ON bs.id = bsj.bookSubjectId
WHERE bs.name = 'fantasia' AND date(a.birthDate) = '2020-01-30'
