-- MySQL dump 10.17  Distrib 10.3.21-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: books
-- ------------------------------------------------------
-- Server version	10.3.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `author`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `birthDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author`
--

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` VALUES (1,'paquito','perez','2020-01-30 15:57:57'),(2,'robert','jordan','2020-01-30 17:00:37'),(3,'por','mi','2020-01-30 20:07:25'),(4,'por','hei','1979-12-05 00:00:00');
/*!40000 ALTER TABLE `author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `isbn` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  PRIMARY KEY (`isbn`),
  KEY `FK_66a4f0f47943a0d99c16ecf90b2` (`authorId`),
  CONSTRAINT `FK_66a4f0f47943a0d99c16ecf90b2` FOREIGN KEY (`authorId`) REFERENCES `author` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES ('9788445007111','test',1),('9788445007113','la rueda del tiempo',2),('9788445007114','la rueda del tiempo',2),('9788445007115','la rueda del tiempo',2),('9788445007116','la rueda del tiempo',2),('9788445007117','la rueda del tiempo',3),('9788445007118','la rueda del tiempo',3),('9788445007119','la rueda del tiempo',4),('9788445007709','el señor de los anillos',1);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_book_subject_book_subject`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_book_subject_book_subject` (
  `bookIsbn` varchar(255) NOT NULL,
  `bookSubjectId` int(11) NOT NULL,
  PRIMARY KEY (`bookIsbn`,`bookSubjectId`),
  KEY `IDX_e757f886795aff52b843b02064` (`bookIsbn`),
  KEY `IDX_0bd821cbfd1cbd2513fc9c5db6` (`bookSubjectId`),
  CONSTRAINT `FK_0bd821cbfd1cbd2513fc9c5db64` FOREIGN KEY (`bookSubjectId`) REFERENCES `book_subject` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_e757f886795aff52b843b020649` FOREIGN KEY (`bookIsbn`) REFERENCES `book` (`isbn`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_book_subject_book_subject`
--

LOCK TABLES `book_book_subject_book_subject` WRITE;
/*!40000 ALTER TABLE `book_book_subject_book_subject` DISABLE KEYS */;
INSERT INTO `book_book_subject_book_subject` VALUES ('9788445007113',1),('9788445007114',1),('9788445007115',1),('9788445007116',1),('9788445007117',1),('9788445007118',1),('9788445007119',1);
/*!40000 ALTER TABLE `book_book_subject_book_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_subject`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_subject`
--

LOCK TABLES `book_subject` WRITE;
/*!40000 ALTER TABLE `book_subject` DISABLE KEYS */;
INSERT INTO `book_subject` VALUES (1,'fantasia');
/*!40000 ALTER TABLE `book_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'books'
--
