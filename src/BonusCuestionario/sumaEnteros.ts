export const sumaEnteros = (numbersList: number[]): number | null => {
    let suma = 0;
    for (let index = 0; index < numbersList.length; index++) {
        if (!Number.isInteger(numbersList[index])) {
            return null;
        }
        suma += numbersList[index];

    }
    return suma;
}
