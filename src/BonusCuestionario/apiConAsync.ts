(() => {
  const getTodos = async () => {
    try {
      const response = await fetch(
        "https://jsonplaceholder.typicode.com/todos/"
      );
      const objectJson = await response.json();
      return console.log(objectJson);
    } catch (err) {
      return console.log(err);
    }
  };

  getTodos();
})();
