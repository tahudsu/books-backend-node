export const palindromo = (palabra): boolean => {
  const checkText = palabra.toLocaleLowerCase().replace(/[\W_]/g, '');

  for (let i = 0; i < checkText.length / 2; i++) {
    if (checkText[i] !== checkText[checkText.length - i - 1]) {
      return false;
    }
  }
  return true;
};
