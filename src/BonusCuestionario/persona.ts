export class Persona {
  name: string;
  surname: string;
  age: string;
  country: string;
  city: string;
  employed: boolean;
  dni: string;

  returnNameNAge(persona: Persona): Persona {
    const notDelete = ["name", "age"];
    Object.keys(persona).forEach(item => {
      if (!notDelete.includes(item)) {
        delete persona[item];
      }
    });

    return persona;
  }

  returnPersonaWithOutEmployedNDni(persona: Persona): Persona {
    const deleteThis = ["employed", "dni"];
    Object.keys(persona).forEach(item => {
      if (deleteThis.includes(item)) {
        delete persona[item];
      }
    });

    return persona;
  }
}
