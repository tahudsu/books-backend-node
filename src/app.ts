import * as express from "express";
import { Request, Response } from "express";
import * as bodyParser from "body-parser";
import { createConnection, ConnectionOptions } from "typeorm";
import bookRoute from "./routes/book";

// create typeorm connection
const connectionOptions: ConnectionOptions = {
  type: "mysql",
  host: process.env.HOST || "localhost",
  port: parseInt(process.env.DB_HOST) || 3306,
  username: process.env.DB_USER || "test",
  password: process.env.DB_PASS || "test",
  database: process.env.DB_NAME || "books",
  entities: ["dist/entity/*.js"],
  logging: process.env.ENVIRONMENT !== 'production',
  synchronize: true
};

createConnection(connectionOptions).then(() => {
  const app = express();
  app.use(bodyParser.json());
  app.use("/v1", bookRoute);
  // register routes

  app.get("/test", async function(req: Request, res: Response) {
    res.json({ test: "hello its me" });
  });

  // start express server
  app.listen(process.env.PORT || 3000, () => {
    console.log("Express server on port 3000: \x1B[32m%s\x1b[0m", "online");
  });
})
.catch(err => console.log(err))  ;
