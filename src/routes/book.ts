import * as express from "express";
import { Request, Response } from "express";

import { Checks } from "../utils/checks";
import { BookPost } from "../entryDto/BookPost.dto";
import BookManager from "../Manager/book";

const app = express();
const bookManager = new BookManager();

/**
 * book by isin
 */
app.get("/book/:isbn", async function(req: Request, res: Response) {
  console.log(req.body);
  console.log(req.params.isbn);
  if (req.params.isbn && Checks.validIsbn(req.params.isbn)) {
    const book = await bookManager.getBook(req.params.isbn);
    return res.send(book);
  }

  return res
    .status(400)
    .send({ message: "values not allowed, check your params" });
});

/**
 * get all books
 */
app.get("/book", async function(req: Request, res: Response) {
  if (req.query.bookSubject) {
    const search = await bookManager.search(req.query);
    return res.send(search);
  }

  const books = await bookManager.getBooks();
  return res.send(books);
});

// create book
app.post("/book", async function(req: Request, res: Response) {
  try {
    if (Checks.checkBodyRequest(req.body)) {
      const bookPost = Object.assign(new BookPost(), req.body);
      const data = await bookManager.save(bookPost);
      return res.send(data);
    }

    throw new Error('not checked values');
  } catch (err) {
    return res
      .status(400)
      .send({ message: "values not allowed, check your params" });
  }
});

export default app;
