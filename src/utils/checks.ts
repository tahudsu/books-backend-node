import { BookPost } from "../entryDto/BookPost.dto";

export const Checks = {
    checkBodyRequest(body: BookPost): boolean {
        if (this.checkBookPost(body)) {
            return true;
        }
        return false;
    },
    checkBookPost(object): object is BookPost {
        if ((object.isbn as BookPost) && 
            (object.title as BookPost) &&
            (object.author as BookPost) &&
            (object.subject as BookPost) &&
            (object.birthDate as BookPost)) {
                return true;
            }
        return false;
    },
    validIsbn(isbnRequested: string): boolean {
        return (//(typeof isbnRequested === 'string')
            isbnRequested.length > 10 && isbnRequested.length < 14);
    }
}
