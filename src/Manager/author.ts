import { Repository, getConnection } from 'typeorm';
import { Author } from '../entity/Author';

const AuthorManager = {
    authorRespository: (): Repository<Author> => getConnection().getRepository(Author),
    async retriveAuthorForBook(book): Promise<Author> {
        const author = book.author.split(" ");

        const authorFind = await this.authorRespository().findOne({ where: { name: author[0], lastName: author[1]}});
        let authorSaveRes; 
        if (!authorFind) {
            const authorSave = Object.assign(new Author(), { name: author[0], lastName: author[1], birthDate: new Date(book.birthDate)})
            authorSaveRes = await this.authorRespository().save(authorSave);
        }

        return authorFind || authorSaveRes;
    }
}

export default AuthorManager;
