import { Book } from '../entity/Book';
import { Repository, getConnection, getManager } from 'typeorm';
import { BookPost } from '../entryDto/BookPost.dto';
import BookSubjectManager from './bookSubject';
import AuthorManager from './author';
class BookManager {
    bookRepository = (): Repository<Book> => getConnection().getRepository(Book);
    async save(book: BookPost): Promise<Book> {
        const bookToSave = Object.assign(new Book(), book);
        const bookSubject = await BookSubjectManager.retrieveSubjectForBooking(bookToSave);
        const author = await AuthorManager.retriveAuthorForBook(book);

        bookToSave.author = author;
        bookToSave.bookSubject = [bookSubject];
        return await this.bookRepository().save(bookToSave);
    }

    async getBooks(): Promise<Book[]> {
        return await this.bookRepository().find({ relations: ["author", "bookSubject"] });
    }

    async getBook(isin): Promise<Book> {
        return await this.bookRepository().findOne(isin, { relations: ["author", "bookSubject"] });
    }

    /**
     * search by book subject and author birthdate if exist
     * @param query 
     */
    async search(query): Promise<Book[]> {
        const sql = await getManager()
            .createQueryBuilder(Book, 'book')
            .leftJoinAndSelect("book.author", "author")
            .leftJoinAndSelect("book.bookSubject", "bookSubject")
            .where("bookSubject.name = :name", { name: query.bookSubject})
        if (query.birthDate) {
            sql.andWhere("date(author.birthDate) = :birthDate", { birthDate: query.birthDate })
        }
        const books =  await sql.getMany();
        return books;
    }
}

export default BookManager;

