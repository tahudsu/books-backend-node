import { Repository, getConnection } from 'typeorm';
import { BookSubject } from '../entity/BookSubject';
const  BookSubjectManager = {
    bookSubjectRepository: (): Repository<BookSubject> => getConnection().getRepository(BookSubject),
    async retrieveSubjectForBooking(book): Promise<BookSubject> {
        const bookSubject = await this.bookSubjectRepository().findOne({ name: book.subject});
        let saveSubject;
        if (!bookSubject) {
            let saveSubject = new BookSubject();
            saveSubject.name = book.subject;
            // saveSubject.books.push();
            saveSubject = await this.bookSubjectRepository().save(saveSubject);
        }

        return bookSubject || saveSubject;
    }
}

export default BookSubjectManager;
