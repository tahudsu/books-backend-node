import { Author } from '../entity/Author';
export class BookPost {
    isbn: string;
    title: string;
    author: string | Author;
    birthDate: string;
    subject: string;
}