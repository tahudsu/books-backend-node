import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { IsDate } from "class-validator";
import { Book } from "./Book";

@Entity()
export class Author {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    lastName: string;

    @Column()
    @IsDate()
    birthDate: Date;

    @OneToMany(type => Book, books => books.author)
    books: Book[]
}