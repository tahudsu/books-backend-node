import { Entity, Column, JoinColumn, PrimaryColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { Author } from "./Author";
import { BookSubject } from './BookSubject';
import { Length } from 'class-validator';

@Entity()
export class Book {
    @PrimaryColumn({ type: "varchar", width: 13, nullable: false, unique: true})
    isbn: string;

    @Column({ nullable: false })
    title: string;

    @ManyToOne(type => Author, author => author.books)
    @JoinColumn()
    author: Author;

    @ManyToMany(type => BookSubject, bookSubject => bookSubject.id,
        {cascade: true})
    @JoinTable()
    bookSubject: BookSubject[];
}